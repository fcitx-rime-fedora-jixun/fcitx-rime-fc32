#!/usr/bin/env bash

export VERSION=v0.3.2-91f378e-3
export ARCHIVE=fcitx-rime-base-${VERSION}

wget https://gitlab.com/fcitx-rime-fedora-jixun/fcitx-rime-base/-/archive/${VERSION}/${ARCHIVE}.tar.gz

tar xzvf ${ARCHIVE}.tar.gz
bash ${ARCHIVE}/build.sh ${pwd}
